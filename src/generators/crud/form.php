<?php
/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
/* @var $generator yii\gii\generators\crud\Generator */
echo $form->field($generator, 'modelClass');
echo $form->field($generator, 'searchModelClass');
echo $form->field($generator, 'controllerClass');
echo $form->field($generator, 'viewPath');
echo $form->field($generator, 'baseControllerClass')->dropDownList([
    'backend\components\AdminController' => 'backend\components\AdminController',
    'yii\web\Controller' => 'yii\web\Controller',
]);
echo $form->field($generator, 'indexWidgetType')->dropDownList([
    'grid' => 'GridView',
    'list' => 'ListView',
]);
echo $form->field($generator, 'enableI18N')->checkbox();
echo $form->field($generator, 'enableGbac')->checkbox();
echo $form->field($generator, 'enableMenu')->checkbox();
echo $form->field($generator, 'enableAdminLog')->checkbox();
echo $form->field($generator, 'messageCategory');
